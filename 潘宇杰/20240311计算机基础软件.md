## 操作系统
ERP：OA、JXC、CRM、CSM

### Window
Win1.1、Win2.1、Win97、Win98、WinXp、Win7、Win8、Win10、Win11、Win12
### Linux（内核）
1、RHEL(红帽子)：
(1)Fedora很流行的桌面Linux系统
(2)Centos分为
旧的centos（RHEL->centos）
新的centos（Centosstream->RHEL）

2、Dabian:
(1)Ubutun(最流行的桌面Linux系统)
(2)kali(黑客系统)
3、麒麟
4、deepin=》深度系统
5、OpenSuSe德国人维护版的操作系统
6、ArchLinux轻量
7、Alpine 容器中轻量的Linux系统
### macOS
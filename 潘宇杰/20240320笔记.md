# 笔记
创建USB启动盘工具：Rufus
操作系统下载：msdn ——> 详细信息 ——> next.itellyou.cn ——> Windows10 企业长期支持版

## 可禁用服务系统
1. Windows Search ： 为文件、电子邮件和其他内容提供内容索引、属性缓存和搜索结果。（如果不是经常要搜索文件的可以禁用）
2. Connected User Experiences and Telemetry:这个服务是微软的一个外围服务，用于收集一些信息，这个服务在某些系统版本中也会出现让CPU满载的情况。
#### 以下四项服务为系统诊断服务，用于支持和执行系统诊断
3. Diagnostic Execution Service
4. Diagnostic Policy Service
5. Diagnostic Service Host
6. Diagnostic System Host


#### 推荐下载Windows操作系统版本
1. Windows10 专业版
2. Windows10 企业长期支持版


### 系统重装前期准备工作
1. 准备一个8Gb以上的U盘或者移动硬盘（下载Rufus软件制作成启动盘）
2. 将原系统中重要文件进行备份
3. 查看自己的电脑位数
4. 下载安装工具
## 用户

+ user abc /add

+ user abc /del

+ user abc /No

### AppData

+ AppData(三个):本地、当地、漫游

### Win10清理软件

+ Cclearner

+ 清理临时文件。使用快捷键Win+R打开运行窗口，输入`%temp%`，删除出现的所有临时文件

+ 清理系统更新文件。右键点击C盘，选择属性，然后点击磁盘清理，在出现的选项中勾选系统更新文件进行删除

+ 清理下载文件。打开运行窗口，输入`C:\Windows\SoftwareDistribution\Download`，删除下载的补丁和安装包等

+ 清理缓存日志文件。打开运行窗口，输入`C:\Windows\System32\logfiles`，删除里面的缓存日志文件

+ 清理Prefetch文件夹。打开运行窗口，输入`prefetch`，删除文件夹中的所有文件

+ 理应用缓存文件。在安装软件时，更改默认安装路径到其他盘，避免在C盘积累垃圾文件

+ 更改默认保存位置。在设置中更改系统默认的文档、图片、视频等文件的保存位置到其他盘

+ 使用第三方清理工具

### 临时文件

+ dism++清理

## 注册表

+ 部分设置(软件安装过程)大部分的设置是被放在win.ini、system.ini等多个初始化ini文件中

+ 软件系统

+ 存储系统和应用程序的配置信息、设置和选项

### 打开注册表的命令是：
1.regedit或regedit.exe

2. 开始-Windows 工具-注册表编辑器

3. 搜索 “注册表编辑器”

### 注册表的数据类型

+ REG_SZ：字符串：文本字符串

+ REG_MULTI_SZ：多字符串值：含有多个文本值的字符串

+ REG_BINARY：二进制数：二进制值，以十六进制显示

+ REG_DWORD：双字值；一个32位的二进制值，显示为8位的十六进制值

Windows 注册表故障中恢复：Restart、Redetect、Restore、Reinstall










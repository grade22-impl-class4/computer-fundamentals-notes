### Linux
Linux，一般指GNU/Linux（单独的Linux内核并不可直接使用，一般搭配GNU套件，故得此称呼），是一种免费使用和自由传播的类UNIX操作系统，其内核由林纳斯·本纳第克特·托瓦兹（Linus Benedict Torvalds）于1991年10月5日首次发布，它主要受到Minix和Unix思想的启发，是一个基于POSIX的多用户、多任务、支持多线程和多CPU的操作系统。它支持32位和64位硬件，能运行主要的Unix工具软件、应用程序和网络协议。

#### 发行版流派
+ Redan
+ Suse Hat
+ Debi
+ Ubuntu



### windows
Windows操作系统是由美国微软公司（Microsoft）开发的一系列操作系统。

#### 发行版流派
+ Windows 1.0
+ Windows 2.0
+ Windows 3.0
+ Windows 95
+ Windows 98
+ Windows Me
+ Windows XP
+ Windows Vista
+ Windows 7
+ Windows 8
+ Windows 10



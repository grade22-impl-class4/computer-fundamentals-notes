### 虚拟机安装Debian 

1. 安装Debian镜像 
2. 下载虚拟机 ,进入虚拟机页面 ,自定义
3. 选择之前下好的 debian 11 iso 文件
4. 自由修改名称和选择虚拟机文件放置的位置 
5. 内存默认 
6. 使用桥接网络 
7. 选择 NAT  
8. 默认
9. 修改虚拟机硬盘空间。一般在 50左右 
10. 设置语言、取名字、设置密码、域名不填 
11. 软件选择:ssh server、标准系统工具 
12. 输入 sudo ifconfig（查看本机 IP）



#### 软件：HEU_KMS Activator、dism++、VMware palyer  

#### 找不到桌面: ——> win + R (打开运行窗口) ——> 输入explorer.exe

+ ISO是光盘镜像的存储格式之一  
+ GHO是ghost工具软件的镜像文件扩展名  
+ WIN镜像是windows基于文件的镜像格式  

### 网络适配器 

+ 桥接模式：直接连接物联网  
+  NAT模式：用于共享主机的IP地址（没有自己的IP地址）  
+  仅主机模式：与主机共享的专用网络  
+ 自定义模式：特定虚拟网络---VMnet(自动桥接)
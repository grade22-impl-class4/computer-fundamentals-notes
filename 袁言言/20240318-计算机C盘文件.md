## C盘主要的文件
C盘通常包含以下重要文件和系统文件夹：
- `Documents and Settings`：存放用户文档和设置。
- `Windows`：包含系统文件和驱动程序，如系统日志、系统自带程序等。
- `Program Files`：默认的安装软件位置，但也可能成为病毒位置，因此需要保护。
- `Program Files (x86)`：用于存放64位程序的文件夹。
- `Users`：存放用户目录，用于储存用户及程序数据。
- `Temp`：临时文件文件夹。
- `Download`：下载文件的默认位置。
- `Desktop`：桌面文件默认位置。


## C盘windows文件夹清理
- 清理临时文件。使用快捷键Win+R打开运行窗口，输入`%temp%`，删除出现的所有临时文件。
- 清理系统更新文件。右键点击C盘，选择属性，然后点击磁盘清理，在出现的选项中勾选系统更新文件进行删除。
- 清理下载文件。打开运行窗口，输入`C:\Windows\SoftwareDistribution\Download`，删除下载的补丁和安装包等。
- 清理缓存日志文件。打开运行窗口，输入`C:\Windows\System32\logfiles`，删除里面的缓存日志文件。
- 清理Prefetch文件夹。打开运行窗口，输入`prefetch`，删除文件夹中的所有文件。
- 清理Windows.old文件夹。这个文件夹包含旧版本的Windows系统文件和用户文件，如果不再需要回滚到旧版本，可以删除。
- 清理应用缓存文件。在安装软件时，更改默认安装路径到其他盘，避免在C盘积累垃圾文件。
- 更改默认保存位置。在设置中更改系统默认的文档、图片、视频等文件的保存位置到其他盘。

在进行清理时，请确保重要数据已备份，避免误删导致数据丢失。

## 注册表编辑器
Windows注册表(Registry)实质上是一个庞大的数据库，它存储着下面这些内容：用户计算机软、硬件的有关配置和状态信息，应用程序和资源管理器外壳的初始条件、首选项和卸载数据；计算机的整个系统的设置和各种许可，文件扩展名与应用程序的关联，硬件的描述、状态和属性；计算机性能记录和底层的系统状态信息，以及各类其他数据。
1. 功能概述
在Windows中，注册表由两个文件组成：System.dat和User.dat（windows 95/98），保存在windows所在的文件夹中。它们是由二进制数据组成。System.dat包含系统硬件和软件的设置，User.dat保存着与用户有关的信息，例如资源管理器的设置，颜色方案以及网络口令等等。

2. 当前用户键
- HKEY_CURRENT_USER（种类：当前用户主键）：记录了有关登录计算机网络的特定用户的设置和配置信息。其子键有：
- AppEvent：与Windows操作系统当中各种特定事件相关连的声音及声音文件所在路径的设置数据。
- Control Panel：包含了一些存储在win.ini及system.ini文件中的数据，并包含了控制面板当中的项目。
- Install_Location_MRU：记录了装载应用程序的驱动器。
- Keyboard Layout：识别普遍有效的键盘配置。
- Network：描述固定网与临时网的连接。
- RemoteAccess：描述了用户拨号连接的详细信息。
- Software：记录了系统程序和用户应用程序的设置。

3. 定位机器键
HKEY_LOCAL_MACHINE（种类：机器主键）：该主键存储了Windows开始运行的全部信息。即插即用设备信息、设备驱动器信息等都通过应用程序存储在此键。子键有：
- Config：记录了计算机的所有可能配置。
- Driver：记录了辅助驱动器的信息。
- Enum：记录了多种外设的硬件标识（ID）、生产厂家、驱动器字母等。
- Hardware：列出了可用的串行口，描述了系统CPU、数字协处理器等信息。
- Network：描述了当前用户使用的网络及登录用户名。
- Security：标识网络安全系统的提供者。
- Software：微软公司的所有应用程序信息都存在於该子键中，包括它们的配置、启动、默认数据。
- System：记录了第一次启动Windows时的大部分部分信息。
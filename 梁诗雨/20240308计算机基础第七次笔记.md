## 以上环境特指Debian操作系统


## 第一步，确认服务器到位，确认具有公网IP
## 第二步，确认域名到位(这里的域名暂定为：www.cyating.top)
## 第三步，将公网Ip和域名绑定，一般至少会解析www.XXX.XXXX、XXX.XXXX
## 第四步，准备nginx服务器，

安装命令：`apt install nginx -y` （如果是centos类的操作系统的话，则是：`yum install nginx -y`）
确认nginx有安装并且正在运行命令：`systemctl status nignx`

## 第五步，在服务器指定路径下准备index.html页面，这个路径建议为：/var/www/www.cyating.top

方式1：本地准备页面

    1. 本地编写index.html
    2. 使用一些工具将写好的index.html文件上传到服务器的指定路径:/var/www

```
    scp 本地路径/index.html root@cyating.top:/var/www/www.cyating.top
```


方式2：远程准备页面

## 第六步 在指定目录，这里是:/etc/nignx/conf.d创建一个nginx的配置文件，
文件名这里为：www.cyating.top.conf
其中的内容如下：

```
server {
    listen 80;
    server_name www.cyating.top;

    location / {
        root /var/www/www.cyating.top;
        index index.html;
    }

}


```
## 第七步 在nginx配置文件搞定的情况下

确认配置文件有没有语法错误：

```

nginx -t
```

让nginx重新加载配置文件

```
nginx -s reload
```




# 总结就是：（Linux服务器环境）部署静态网站，就只围绕两个东西来转：一个是nginx的安装配置，一个是已经存在的网站


/etc/nginx/conf.d

配置文件：abc.qq.com.conf

/var/www

abc.qq.com/index.html
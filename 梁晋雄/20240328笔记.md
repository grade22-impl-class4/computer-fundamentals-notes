### 封装

1.win系统母盘

2.pe系统（也是一个[iso镜像](https://www.zhihu.com/search?q=iso镜像&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A101309106})）

3.封装工具Easy Sysprep

4.系统备份工具Easy Image X（封装好后，用来做[gho系统](https://www.zhihu.com/search?q=gho系统&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A101309106})文件）

5.驱动，各种驱动，比如it天空的

6.运行库（.Net   Dx9  vc

7.用于[打补丁](https://www.zhihu.com/search?q=打补丁&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A101309106})的软件，网上找，最新的

8.什么系统瘦身，[注册表](https://www.zhihu.com/search?q=注册表&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A101309106})清理，系统优化各种清理软件，目的是为了让系统更干净。

差不多了，这些东西有了以后，直接考虑在[虚拟机](https://www.zhihu.com/search?q=虚拟机&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A101309106})上运作。

9.装好系统母盘并且是管理员帐号

10.将之前准备的东西放在[非系统盘](https://www.zhihu.com/search?q=非系统盘&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A101309106})里

11.打补丁，就是网上下载的最新补丁

12.控制面板和计算机属性里按个人喜好设置好，删除其他帐号，只留管理员帐号

13.预装自己的软件，比如qq什么的

14.各种系统瘦身，注册表清理，系统优化什么的，期间会重启几次

15.在c盘（系统盘）里新建一个文件夹Sysprep并把驱动和[运行库](https://www.zhihu.com/search?q=运行库&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A101309106})放进去

16.开始封装，先打开Easy Sysprep第一阶段，自己写好第一阶段，然后执行，完成后关机

17.进入pe，就是光盘映像载入下载好的pe.iso

18.进入Easy Sysprep，驱动和运行库里，选择事先准备好放在c盘的驱动和运行库。

19.完成后运行Easy Image X 备份c盘系统
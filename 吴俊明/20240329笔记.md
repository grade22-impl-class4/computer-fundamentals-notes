## **第一阶段**

在C盘目录下新建文件夹Sysprep，拷贝万能驱动助理、VC、DirectX到该目录下，如果在之前已经安装过运行库，VC、DirectX可以不用拷贝。

注：在C盘目录下建Sysprep，待系统安装完后，可以自动删除Sysprep文件夹。





![img](https://pic3.zhimg.com/80/v2-853c2fdf2fdf981041a3046175223aa2_720w.webp)



运行【Easy Sysprep】，如果是第一次使用，配置显示为空，如果之前有用过，会显示上一次的配置





![img](https://pic1.zhimg.com/80/v2-b0d3da46577bb733bd357eeb334d9a84_720w.webp)





说明：

【一键封装】快捷的一键封装操作

\- 当没有指定【配置文件】时，会按照全默认的方式执行系统封装，适合新人；

\- 当指定【配置文件】时，会以配置文件所设置的方式执行封装，而无需进入设置页面，适合熟手。

【设置】进入设置界面，指定封装的各个参数。

【配置文件】封装完成后，所有设置会自动记录为配置文件，便于今后加载。

进入设置中，可根据提示自行调整，调整完后，点【封装】，





![img](https://pic4.zhimg.com/80/v2-bceca4914fa0c5b8733acb7ed9cdee0b_720w.webp)





注意：封装完成后，如果还需要调整某些东西，那选项就选择【退出本程序】，否则一旦重启，就不可以再进行修改。





![img](https://pic2.zhimg.com/80/v2-ef70c28561d6d5968e56c735383c2709_720w.webp)





Easy Sysprep的第一阶段封装只涉及封装所必须的操作，所以操作较少，更多的调整操作留给了第二阶段。

**小提示：为了安全着想，可以在第一阶段完后，关机，拍个快照**

## **第二阶段**

Easy Sysprep的第二阶段封装在PE环境下完成，而非常规的系统桌面环境。第二阶段的重点在于：调整系统配置。

重启电脑后，进入PE系统





![img](https://pic4.zhimg.com/80/v2-de9bb5419cfae60c8f9be2ba4c9340eb_720w.webp)







![img](https://pic1.zhimg.com/80/v2-fce1ebaf73afffce46ceab69f355f814_720w.webp)





在优化界面可直接全选，这是官方较为中庸的优化项目，大家也可以根据实际选用优化内容





![img](https://pic3.zhimg.com/80/v2-867bcb3275fceef84b07b756e0d889a2_720w.webp)







![img](https://pic1.zhimg.com/80/v2-296dc62df9173eae5a2d93b338eb16ec_720w.webp)







![img](https://pic4.zhimg.com/80/v2-72922ae69aba147a55b8975b2754c5e3_720w.webp)







![img](https://pic4.zhimg.com/80/v2-a6d812b431a15fa510a9f5d790bde2f7_720w.webp)







![img](https://pic4.zhimg.com/80/v2-c2eab97f97d27cb7f95c36f227ab16cb_720w.webp)





![img](https://pic1.zhimg.com/80/v2-6561d7c0136d993be4d45f104a142504_720w.webp)



```text
说明：
【时机】部署前/部署中/部署后/登录时/进桌面。
【任务】
 - 运行：可调用.exe/.cmd/.bat/.msi/.vbs/.ps1/.au3/.a3x执行，并可调用.reg导入；
 - 运行(隐藏)：隐藏调用
 - 运行(不等待)：调用后不等待其执行完毕就开始执行下一个任务（慎用！）
 - 删文件：删除指定文件
 - 删文件夹：删除指定文件夹
【路径】指定所执行任务的完整路径，并可使用指代盘符
 - %SystemDrive%：系统分区
 - %X%：所有类型分区
 - %UDisk%：所有U盘
 - %CDROM%：所有光盘
```





![img](https://pic3.zhimg.com/80/v2-d9faf061351544a409f6b6bb37e8dfc2_720w.webp)



至此，系统封装已结束，我们要保存劳动成果。打开Easy Image X，把封装好的系统打成【.gho】，备份完了，就可以重启查看自己的成果了，当你想要把GHOST文件拷出来用于其他电脑，参考【虚拟机与主机之间的文件交换】。





![img](https://pic4.zhimg.com/80/v2-0cc75ac448876ada544b1d45c4ba62cf_720w.webp)







![img](https://pic3.zhimg.com/80/v2-d4213849c46c47ff572d3f7617b230ca_720w.webp)







![img](https://pic4.zhimg.com/80/v2-b1bdf97fb589d67d372503b9427e8de7_720w.webp)



## **至此，整个Window10的封装已经完成，大家可能在开始学的时候会很困难，会出现很多错误，会做出不理想的GHOST，但这没关系，只要大家多操作几次，把一些问题理清楚，那接下来你就会做出你想要的GHOST。**
## Office

### 工具介绍 :

Office Tool Plus 是目前颇为优秀的 Office 部署工具，工具基于 Office 部署工具制作，内置迅雷、OSPP 等多个工具，方便用户下载、安装、激活、管理 Office

### 下载地址 : 

https://link.zhihu.com/?target=https%3A//otp.landian.vip/zh-cn/download.html

### 怎么使用 Office Tool Plus : 

1. 打开 Office Tool Plus 官网，有三个版本选择，推荐选择第一个【包含框架】单击下载 

2. 解压文件后直接双击 RunMe.bat 以运行 Office Tool Plus，建议右击【以管理员身份运行】

3. 接受许可条例

4. 今天软件界面后选择【部署】

5. 部署界面看到很多设置，可以看到有三大板块，一是基础设置、二是高级设置

### 安装前需要注意以下事项：

1. 卸载 WPS，两者共存容易出问题

2. 卸载旧版本 Office，否则有可能无法安装

3. 清除旧版本激活信息，Windows 10 上必须做(激活页面 -> 许可证管理 -> 清除激活状态)

4. 清除 Office 设置，可选(工具箱页面 -> 恢复 Office 为默认设置)

### 基础设置如下 : 

1. 选择 Office 版本，建议选择【批量版】，同时选择对应的通道【企业长期版】，否则会无法安装

2. 添加语言

3. 体系结构：32 位兼容性好；64 位兼容性稍差且体积大，但处理大数据能力稍强

4. 通道：选“当前通道”就行，Office 2019/2021 批量版请选择 Office 2019/2021 企业长期版

5. 部署模式：那肯定要选安装啊，要不然怎么安装

6. 安装模块：如没有特殊要求，请勿选择 Office Tool Plus

7. 一切基础设置完成后单击【开始部署】，部署时请耐心等待，不要强制结束安装程序或重启计算机

### 激活工具 ：

​​HEU KMS Activator (是一款用于Windows和Office激活的工具)

### 虚拟机 : VMware-player-full

### C盘清理软件 : Dism ++

### 桌面不显示应用图标 :

重启资源管理器。按下Ctrl+Alt+Delete键打开任务管理器，点击“文件”，选择“运行新任务”，输入“explorer.exe”后点击“确定”
#  基于Debian操作系统 
## 1.确认公网IP
## 2.确认域名到位
## 3.将公网IP跟域名绑定
## 4.准备Nginx服务器，安装：apt install nginx-y
1.如果是centos类操作系统：yum install nginx-y
2.确认Nginx系统安装并在执行：sysemcll status nginx
## 5.在服务器指定的路径下面准备Idenx.html页面 路径为：/var /www/www.域名
`scp 本地路径 / idenx.html root@域名 ： /var /www/www.域名`
## 6. 在指定目录/etc/nginx.conf.d 创建一个Nginx的配置文件
`文件名 ： www.域名.conf`
## 7.在nginx搞定的情况下
1.确认有没有语法错误：nginx-t
2.让nginx重新加载配置文件： nginx -s reload


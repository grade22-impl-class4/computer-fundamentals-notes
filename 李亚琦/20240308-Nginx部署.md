# 配置 Nginx

默认方式安装的软件，配置一般都在 /etc 目录下
可以通过 `vim /etc/nginx/nginx.conf` 直接编辑 Nginx 主配置文件（不推荐）：在 http 模块中，添加如下 server 配置内容：

或者在 /etc/nginx/conf.d 目录下新建 xxxx.xxx.conf 配置文件（推荐），写入如下内容：

```nginx
server{
    listen 80; # 指定 80 端口
    server_name xxxx.xxx; # 指定域名
    location / {
        root /home/www/xxxx.xxx; # 指定静态网站根目录
        index index.html; # 指定默认访问文件
    }
}
```
## 计算机基础
### 硬件
1. 内存:主板上的存储部件,内存的容量大小可以影响到程序的运行的程序.
2. CPU:一块超大规模的集成电路,是一台计算机的运算核心和控制核心,CPU主要包括运算器和高速缓冲储存器一级它们之间联系的数据、控制及状态的总线.
3. 硬盘:计算机最主要的存储设备
### 软件
1. 系统软件
2. 应用软件
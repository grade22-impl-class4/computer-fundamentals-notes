## 域名和公网ip绑定
## 下载Nginx
安装命令 apt install nginx -y
## 在Nginx下配置一个index.html文件
  scp 本地路径/index.html root@cyating.top:/var/www/www.cyating.top
  创建一个nginx的配置文件：
  server {
    listen 80;
    server_name www.cyating.top（解析的域名）;

    location / {
        root /var/www/www.cyating.top;
        index index.html;
    }

}
总结：部署围绕着Nginx安装配置，一个已存在的网站


## 第七步 在nginx配置文件搞定的情况下

确认配置文件有没有语法错误：
```
nginx -t
```
让nginx重新加载配置文件
```
nginx -s reload
```

# 总结就是：（Linux服务器环境）部署静态网站，就只围绕两个东西来转：一个是nginx的安装配置，一个是已经存在的网站
/etc/nginx/conf.d
配置文件：abc.qq.com.conf
/var/www
abc.qq.com/index.html

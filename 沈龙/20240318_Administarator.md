### AppData

1. Local 文件夹用于存储特定于单个 Windows 系统的数据，这意味着数据不会在多台 PC 之间同步

2. LocalLow 文件夹与Local 文件夹基本相同，区别在于前者用于优先级较低的应用程序（运行时安全设置受限）

3. Roaming 文件夹用于存储将在多个 Windows 系统之间同步的数据。这通常用于存储设置，例如：书签，保存的3. 密码等

### 管理用户

1. net user aaa /add 新增

2. net user aaa /active:yes|no 管理账户是否禁用

3. net user aaa /del 删除

### 注册表

1. 打开注册表 regedit
